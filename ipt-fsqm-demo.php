<?php
/*
Plugin Name: Demo Generator for eForm Plugin
Plugin URI: https://iptms.co/eform
Description: Builds demo for using with eForm - WordPress Form Builder Plugin
Author: iPanelThemes
Version: 3.0.0
Author URI: http://ipanelthemes.com/
License: GPLv3
Text Domain: ipt_fsqm
*/

/**
 * Copyright iPanelThemes.com, 2016
 * This WordPress Plugin is comprised of two parts: (1) The PHP code and integrated
 * HTML are licensed under the GPL license as is WordPress itself.
 * You will find a copy of the license text in the same directory as this text file.
 * Or you can read it here: http://wordpress.org/about/gpl/
 * (2) All other parts of the theme including,
 * but not limited to the CSS code, images, and design are licensed according to
 * the license purchased. Read about licensing details here:
 * http://themeforest.net/licenses/regular_extended
 */

if ( is_admin() ) {
	add_action( 'plugins_loaded', 'ipt_fsqm_demo_admin_init', 10 );
}

function ipt_fsqm_demo_admin_init() {
	if ( class_exists( 'IPT_FSQM_Loader' ) ) {
		include_once dirname( __FILE__ ) . '/classes/class-ipt-fsqm-demo.php';
		add_filter( 'ipt_fsqm_admin_menus', array( 'IPT_FSQM_Demo', 'filter' ) );
	}
}

function ipt_fsqm_demo_install() {
	if ( ! class_exists( 'IPT_FSQM_Loader' ) || ! class_exists( 'IPT_FSQM_Admin_Base' ) ) {
		deactivate_plugins( __FILE__ );
		wp_die( __( 'Sorry WP Feedback, Survey & Quiz Manager - Pro needs to be installed', 'ipt_fsqm' ), __( 'Can not activate', 'ipt_fsqm' ) );
	}
}
register_activation_hook( __FILE__, 'ipt_fsqm_demo_install' );
