<?php

class IPT_FSQM_Demo extends IPT_FSQM_Admin_Base {
	public static function filter( $admin_classes ) {
		$admin_classes[] = 'IPT_FSQM_Demo';
		return $admin_classes;
	}

	public function __construct() {
		$this->capability = 'manage_feedback';
		$this->action_nonce = 'ipt_fsqm_demo_nonce';

		parent::__construct();

		$this->icon = 'stack';
		add_action( 'wp_ajax_ipt_fsqm_demo_ajax', array( $this, 'ajax_response' ) );
	}

	public function admin_menu() {
		$this->pagehook = add_submenu_page( 'ipt_fsqm_dashboard', __( 'IPT FSQM Demo Generator', 'ipt_fsqm' ), __( 'Demo Generator', 'ipt_fsqm' ), $this->capability, 'ipt_fsqm_demo_generator', array( $this, 'index' ) );
		parent::admin_menu();
	}

	public function index() {
		$this->index_head( __( 'WP Feedback, Survey & Quiz Manager - Pro <span class="icon-arrow-right-2"></span> Demo Generator', 'ipt_fsqm' ), false );

		if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
			$this->ajax_form();
		} else {
			$this->ajax_options();
		}

		$this->index_foot( false );
	}

	protected function ajax_form() {
		$this->ui->progressbar( 'ipt_fsqm_demo_pb', 0 );
		$this->ui->ajax_loader( false, 'ipt_fsqm_demo_loader', array(), true, __( 'Please wait', 'ipt_fsqm' ) );
		?>
<table class="ipt_fsqm_preview" id="ipt_fsqm_demo_log" style="display: none;">
	<thead>
		<tr>
			<th><?php _e( 'Quick Preview', 'ipt_fsqm' ); ?></th>
			<th><?php _e( 'RAW Log Data', 'ipt_fsqm' ); ?></th>
		</tr>
	</thead>
	<tbody>

	</tbody>
</table>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		var past_days = <?php echo (int) $this->post['ipt_fsqm_demo_past_days']; ?>;
		var form_id = <?php echo (int) $this->post['ipt_fsqm_demo_form_id']; ?>;
		var max = <?php echo (int) $this->post['ipt_fsqm_demo_max']; ?>;
		var _wpnonce = '<?php echo wp_create_nonce( 'ipt_fsqm_demo_nonce' ); ?>';
		var percent = 0;
		var progressbar = $('#ipt_fsqm_demo_pb'),
		loader = $('#ipt_fsqm_demo_loader'),
		table = $('#ipt_fsqm_demo_log'),
		table_body = table.find('tbody'),
		total_inserted = 0;

		function IPT_FSQM_Demo_JS(day) {
			var data = {
				action: 'ipt_fsqm_demo_ajax',
				form_id: form_id,
				day: day,
				max: max,
				_wpnonce: _wpnonce
			};

			$.post(ajaxurl, data, function(obj) {
				if ( obj == null ) {
					loader.find('.ipt_uif_ajax_loader_text').html('Server Side Error');
					return;
				}
				percent = ((day + 1)/past_days) * 100;
				var p_now = Math.round(percent);
				if(p_now > 100) {
					p_now = 100;
				}
				progressbar.progressbar('value', percent);
				progressbar.find('.ipt_uif_progress_value').html(p_now + '%');

				if ( obj.max_inserts !== undefined ) {
					table_body.append('<tr class="head"><th>' + obj.msg + '</th><th>Number of Submissions: ' + obj.max_inserts + '</th></tr>');
					total_inserted += obj.max_inserts;
				}

				if ( obj.log !== undefined ) {
					for(var i = 0; i < obj.log.length; i++) {
						table_body.append('<tr><th>' + obj.log[i].link + '</th><td>' + JSON.stringify(obj.log[i].data_api) + '</td></tr>');
					}
				}

				if((day + 1) >= past_days) {
					loader.find('.ipt_uif_ajax_loader_text').html('' + total_inserted + ' Submissions');
					loader.find('.ipt_uif_ajax_loader_inner').removeClass('ipt_uif_ajax_loader_animate');
					progressbar.hide();
					table.slideDown('normal');
					$(document).iptPluginUIFAdmin('reinitTBAnchors');
				} else {
					loader.find('.ipt_uif_ajax_loader_text').html(obj.msg);
					IPT_FSQM_Demo_JS(++day);
				}

			}, 'json').fail(function() {
				loader.find('.ipt_uif_ajax_loader_text').html('AJAX Error');
			});
		}

		IPT_FSQM_Demo_JS(0);
	});
</script>
		<?php
	}

	/**
	 *
	 * @global wpdb $wpdb
	 * @global array $ipt_fsqm_info
	 */
	public function ajax_response() {
		global $wpdb, $ipt_fsqm_info;

		// Get the variables
		$form_id = $this->post['form_id'];
		$day = $this->post['day'];
		$max = $this->post['max'];
		$return = array();

		// Check the nonce
		if ( ! wp_verify_nonce( $_POST['_wpnonce'], 'ipt_fsqm_demo_nonce' ) ) {
			$return['msg'] = __( 'Cheating?', 'ipt_fsqm' );
			echo json_encode( $return );
			die();
		}

		// Init the form
		$form = new IPT_FSQM_Form_Elements_Data( null, $form_id );
		if ( null == $form->form_id ) {
			$return['msg'] = __( 'Invalid Form ID', 'ipt_fsqm' );
			echo json_encode( $return );
			die();
		}

		// Init the insert date
		$today = current_time( 'timestamp' );
		$date = date( 'Y-m-d', mktime( 0, 0, 0, date( 'm', $today ), date( 'd', $today ) - $day, date( 'Y', $today ) ) );

		// Check the max insert
		if ( $max < 1 ) {
			$max = 1;
		}
		$max_insert = mt_rand( 1, (int) $max );
		$return['max_inserts'] = $max_insert;
		$return['log'] = array();
		for ( $it = 0; $it < $max_insert; $it++ ) {
			// Reset the data
			$form->data_id = null;
			$form->prepare_empty_data();
			$form->data->user_id = 0;
			$form->data->ip = $this->get_random_ip();
			$form->data->star = rand( 0,1 );

			$i_date = sprintf( '%s %02d:%02d:%02d', $date, rand( 0, 23 ), rand( 0, 59 ), rand( 0, 59 ) );
			$form->data->date = $i_date;

			// Process the pinfo
			$i_pinfo = array();
			$f_name = $this->get_random_f_name();
			$l_name = $this->get_random_l_name();
			$email = strtolower( $f_name ) . '@' . $this->get_random_e_domain();
			$phone = $this->get_random_phone();
			foreach ( $form->pinfo as $p_key => $pinfo ) {
				$data = $form->get_submission_structure( $pinfo['type'] );
				switch ( $pinfo['type'] ) {
					case 'f_name' :
						$data['value'] = $f_name;
						break;
					case 'l_name' :
						$data['value'] = $l_name;
						break;
					case 'email' :
						$data['value'] = $email;
						break;
					case 'phone' :
						$data['value'] = $phone;
						break;
					case 'p_name' :
						$data['value'] = $this->get_random_name();
						break;
					case 'p_email' :
						$data['value'] = $this->get_random_email();
						break;
					case 'p_phone' :
						$data['value'] = $this->get_random_phone();
						break;
					case 'textinput' :
					case 'keypad' :
						switch ( $pinfo['validation']['filters']['type'] ) {
							default :
							case 'all' :
								$data['value'] = $this->get_random_text();
								break;
							case 'phone' :
								$data['value'] = $this->get_random_phone();
								break;
							case 'url' :
								$data['value'] = $this->get_random_url();
								break;
							case 'email' :
								$data['value'] = $this->get_random_email();
								break;
							case 'ipv4' :
								$data['value'] = $this->get_random_ip();
								break;
							case 'number' :
							case 'integer' :
								$data['value'] = $this->get_random_number( $pinfo['validation']['filters']['min'], $pinfo['validation']['filters']['max'] );
								break;
							case 'onlyNumberSp' :
								$data['value'] = $this->get_random_limited_text( $pinfo['validation']['filters']['minSize'], $pinfo['validation']['filters']['maxSize'], 'number' );
								break;
							case 'onlyLetterNumberSp' :
							case 'onlyLetterSp' :
							case 'noSpecialCharacter' :
								$data['value'] = $this->get_random_limited_text( $pinfo['validation']['filters']['minSize'], $pinfo['validation']['filters']['maxSize'] );
								break;
							case 'personName' :
								$data['value'] = $this->get_random_name();
						}
						break;
					case 'textarea' :
						$data['value'] = $this->get_random_multiline_text();
					case 'guestblog' :
						$data['value'] = $this->get_random_multiline_text();
						$data['taxonomy'] = array();
						$data['bio'] = $this->get_random_name();
						$data['title'] = $this->get_random_text();
					case 'password' :
						$data['value'] = $this->get_random_password( rand( 0, 10 ) );
						break;
					case 'p_radio' :
					case 'p_checkbox' :
					case 'p_select' :
						$data = $this->generate_random_mcq( $pinfo, $data );
						break;
					case 's_checkbox' :
						$data['value'] = true;
						break;
					case 'address' :
						$data['values'] = $this->get_random_address();
						break;
					case 'datetime' :
						$data['value'] = $this->get_random_date( $pinfo['validation']['filters']['past'], $pinfo['validation']['filters']['future'] );
						break;
					case 'p_sorting' :
						$data = $this->generate_random_sorting( $pinfo, $data );
						break;
					case 'payment' :
						// Assumes we are giving stripe payment
						// Always a $100
						// Also a test API key for stripe is provided
						// Otherwise it will generate a payment failed notice
						// Although the submission will get saved
						$data = array(
							'm_type' => 'pinfo',
							'type' => 'payment',
							'value' => '100',
							'coupon' => '',
							'couponval' => '',
							'pmethod' => 'stripe',
						);

						// We need to clone it to the post vars too
						// Because FSQM expects it
						$form->post[ 'ipt_fsqm_form_' . $form->form_id ]['pinfo'][ $p_key ] = array(
							'm_type' => 'pinfo',
							'type' => 'payment',
							'value' => '100',
							'coupon' => '',
							'couponval' => '',
							'pmethod' => 'stripe',
							// Along with dummy CC info
							'cc' => array(
								'ctype' => 'visa',
								'number' => '4242 4242 4242 4242',
								'name' => 'John Doe',
								'expiry' => '09/' . ( date( 'y' ) + 10 ),
								'cvc' => mt_rand( 0, 5 ) . mt_rand( 4, 8 ) . mt_rand( 0, 9 ),
							),
						);
						break;
				}

				$i_pinfo[ $p_key ] = $data;
			}
			$form->data->pinfo = $i_pinfo;

			// Do the mcq
			$i_mcq = array();
			foreach ( $form->mcq as $m_key => $mcq ) {
				$data = $form->get_submission_structure( $mcq['type'] );
				switch ( $mcq['type'] ) {
					case 'checkbox' :
					case 'radio' :
					case 'select' :
						$data = $this->generate_random_mcq( $mcq, $data );
						break;
					case 'thumbselect' :
						$data['options'] = $this->get_random_thumbselect( $mcq );
						break;
					case 'slider' :
						$data['value'] = $this->generate_slider_value( $mcq['settings']['min'], $mcq['settings']['max'], $mcq['settings']['step'] );
						break;
					case 'range' :
						$data['values'] = $this->generate_range_value( $mcq['settings']['min'], $mcq['settings']['max'], $mcq['settings']['step'] );
						break;
					case 'grading' :
						foreach ( $mcq['settings']['options'] as $o_key => $op ) {
							$data['options'][ $o_key ] = (true == $mcq['settings']['range'] ? $this->generate_range_value( $mcq['settings']['min'], $mcq['settings']['max'], $mcq['settings']['step'] ) : $this->generate_slider_value( $mcq['settings']['min'], $mcq['settings']['max'], $mcq['settings']['step'] ));
						}
						break;
					case 'spinners' :
						foreach ( $mcq['settings']['options'] as $o_key => $op ) {
							$data['options'][ $o_key ] = $this->generate_slider_value( $mcq['settings']['min'], $mcq['settings']['max'], $mcq['settings']['step'] );
						}
						break;
					case 'starrating' :
					case 'scalerating' :
						foreach ( $mcq['settings']['options'] as $o_key => $op ) {
							$data['options'][ $o_key ] = $this->generate_slider_value( 1, $mcq['settings']['max'], 1 );
						}
						break;
					case 'matrix' :
						$col_keys = array_keys( $mcq['settings']['columns'] );
						foreach ( $col_keys as $c => $k ) {
							$col_keys[ $c ] = (string) $k;
						}
						foreach ( $mcq['settings']['rows'] as $r_key => $row ) {
							$data['rows'][ $r_key ] = array();
							if ( true == $mcq['settings']['multiple'] ) {
								$tmp_col_keys = $col_keys;
								shuffle( $tmp_col_keys );
								$max_checkboxes = rand( 1, count( $tmp_col_keys ) );
								for ( $i = 0; $i < $max_checkboxes; $i++ ) {
									$data['rows'][ $r_key ][] = array_pop( $tmp_col_keys );
								}
							} else {
								$data['rows'][ $r_key ][] = $col_keys[ rand( 0, count( $col_keys ) - 1 ) ];
							}
						}
						break;
					case 'matrix_dropdown' :
						foreach ( $mcq['settings']['rows'] as $r_key => $row ) {
							$data['rows'][ "$r_key" ] = array();
							foreach ( $mcq['settings']['columns'] as $c_key => $column ) {
								$data['rows'][ "$r_key" ][ "$c_key" ] = (string) array_rand( $mcq['settings']['options'] );
							}
						}
						break;
					case 'likedislike' :
						// Set the like or dislike
						$data['value'] = ( 1 == mt_rand( 0, 1 ) ) ? 'like' : 'dislike';
						// Set the feedback too
						if ( true == $mcq['settings']['show_feedback'] ) {
							$data['feedback'] = $this->get_random_multiline_text();
						}
						break;
					case 'smileyrating' :
						// Set the selected smiley
						$enabled_smilies = array();
						foreach ( $mcq['settings']['enabled'] as $key => $enb ) {
							if ( true === $enb ) {
								$enabled_smilies[] = $key;
							}
						}
						shuffle( $enabled_smilies );
						$data['option'] = array_pop( $enabled_smilies );
						// Set the feedback too
						if ( true == $mcq['settings']['show_feedback'] ) {
							$data['feedback'] = $this->get_random_multiline_text();
						}
						break;
					case 'toggle' :
						$data['value'] = (bool) rand( 0, 1 );
						break;
					case 'sorting' :
						$data = $this->generate_random_sorting( $mcq, $data );
						break;
				}
				$i_mcq[ $m_key ] = $data;
			}
			$form->data->mcq = $i_mcq;

			// Do the freetype
			$i_freetype = array();
			foreach ( $form->freetype as $f_key => $freetype ) {
				$data = $form->get_element_structure( $freetype['type'] );

				switch ( $freetype['type'] ) {
					case 'feedback_large':
						$data['value'] = $this->get_random_multiline_text();
						break;
					case 'feedback_small' :
						$data['value'] = $this->get_random_text();
						break;
					case 'upload' :
						// This is actually difficult
						// because would involve copying files
						// adding them to database then assigning id
						// Let us just simply give a bogus ID hoping it would pass
						$data['id'] = array( '0' );
						break;
					case 'mathematical' :
						// Just give a random value
						// Who cares
						$data['value'] = mt_rand( 100, 1000 );
						break;
					case 'feedback_matrix' :
						// We put text inside rows
						foreach ( $freetype['settings']['rows'] as $r_key => $row ) {
							$data['rows'][ "$r_key" ] = array();
							foreach ( $freetype['settings']['columns'] as $c_key => $column ) {
								if ( true == $freetype['settings']['multiline'] ) {
									$data['rows'][ "$r_key" ][ "$c_key" ] = $this->get_random_multiline_text();
								} else {
									$data['rows'][ "$r_key" ][ "$c_key" ] = $this->get_random_text();
								}
							}
						}
						break;
					case 'gps' :
						$data = array_merge( $data, $this->get_random_gps() );
						break;
					case 'signature' :
						$data['value'] = 'image/jsignature;base30,2R3335735100Z323244533231221Ya73634_3NZ3234542Y49696676aa576444452Z42435_3P_1O_5y0Z433653212001_ofihl1umjhfb875_4M43376677454311211000Z153444346455445_1EZ442310Y16477a7a8a7556463433312100Z112_7wZ3364343000Y25652523010000Z500000Y1123335000000003351200000Z2100Y22332233311210124154232000001Z333130000Y12323555447446644000Z11333245522000Y1354845334553Z7_4J47aa8a865542Z14468869a767Ya9ab98866654Za669a7a7532Y367566676Za98dcb834Y46a6686665742Z249b6ab7642Y256a98989656320Z224876efejkhgfeb88721Y18hlkplljfh98533323Z2_cX2402120000000_1R5bcdbfffdbca5_cH000032557ca8a4_3CZ666988a7695511_eR00010000000000010000121112234644423223316775543231000Z164110000Y3243366958643_ddbfhiklijfb9c55Z79bdcb8845344Y58786745545510Z457768866674Y3b9cbba78753210Z424423_jw586641210100Z124110000Y4647555532311201000Z2000Y33242364644341335746643210Z7957552000Y225ba77976662431Z52300Y377acda85Z659a98865_4I032474a9867943Z4698da98b47724Y16956a857595Z3a579876330Y8ca865533Z6a7aa7a4a6Y19778a8a58631Z4757baa9d8984Y96aa577546541231000Z1222';
						break;
				}

				$i_freetype[ $f_key ] = $data;
			}

			$form->data->freetype = $i_freetype;
			$log = array();
			$form->send_mail = false;
			$form->doing_update = false;
			$log['data_api'] = $form->process_save();
			$log['link'] = '<a href="admin-ajax.php?action=ipt_fsqm_quick_preview&id=' . $form->data_id . '" class="thickbox">' . $form->data->f_name . ' ' . $form->data->l_name . ' ' . '</a>';
			$return['log'][] = $log;
		}

		// Return
		$return['msg'] = sprintf( __( 'Done for %s', 'ipt_fsqm' ), $date );
		echo json_encode( (object) $return );
		die();
	}

	protected function generate_slider_value( $min, $max, $step ) {
		$min = (string) $min;
		$max = (string) $max;
		$step = (string) $step;
		if ( '' === $min ) {
			$min = 0;
		}
		if ( '' === $max ) {
			$max = 100;
		}
		if ( '' === $step ) {
			$step = 1;
		}

		// Select between a maximum of 4 values, else it is just too much
		$randoms = array();
		$difference = ceil( ($max - $min) / (3 * $step) );
		if ( $difference == 0 ) {
			$difference = $step;
		}
		for ( $i = 0; $i <= 3; $i++ ) {
			$randoms[] = $min + $difference * $i * $step;
		}
		$val = $randoms[ rand( 0, count( $randoms ) - 1 ) ];
		if ( $val < $min ) {
			$val = $min;
		}
		if ( $val > $max ) {
			$val = $max;
		}
		return (string) $val;
	}

	protected function generate_range_value( $min, $max, $step ) {
		if ( '' == $min ) {
			$min = 0;
		}
		if ( '' == $max ) {
			$max = 100;
		}
		if ( '' == $step ) {
			$step = 1;
		}
		$val = array(
			'min' => $min,
			'max' => $max,
		);
		$min_max = floor( ($min + $max) / 2 );
		$val['min'] = $this->generate_slider_value( $min, $min_max, $step );
		$val['max'] = $this->generate_slider_value( $min_max, $max, $step );
		return $val;
	}

	protected function generate_random_sorting( $element, $data ) {
		$option_keys = array_keys( $element['settings']['options'] );
		foreach ( $option_keys as $key => $okey ) {
			$option_keys[ $key ] = (string) $okey;
		}
		$preset = rand( 0, 3 );
		if ( $preset != 0 ) {
			$randoms = array();

			// Reverse it
			$randoms[0] = array_reverse( $option_keys );

			// Half move up or down
			$randoms[1] = array();
			$randoms[2] = array();
			for ( $i = 0; $i < count( $option_keys ); $i++ ) {
				if ( $i % 2 == 0 ) {
					$randoms[1][] = $option_keys[ $i ];
				} else {
					$randoms[2][] = $option_keys[ $i ];
				}
			}
			for ( $i = 0; $i < count( $option_keys ); $i++ ) {
				if ( $i % 2 == 0 ) {
					$randoms[2][] = $option_keys[ $i ];
				} else {
					$randoms[1][] = $option_keys[ $i ];
				}
			}

			$option_keys = $randoms[ rand( 0, count( $randoms ) - 1 ) ];
		}
		$data['order'] = $option_keys;
		return $data;
	}

	protected function generate_random_mcq( $element, $data ) {
		$option_keys = array_keys( $element['settings']['options'] );
		foreach ( $option_keys as $key => $okey ) {
			$option_keys[ $key ] = (string) $okey;
		}

		if ( isset( $element['settings']['others'] ) && true == $element['settings']['others'] ) {
			$option_keys[] = 'others';
		}

		shuffle( $option_keys );

		if ( $element['type'] == 'checkbox' || $element['type'] == 'p_checkbox' ) {
			$min_checkboxes = 2;
			$max_checkboxes = count( $option_keys );
			if ( isset( $element['validation']['filters'] ) ) {
				if ( isset( $element['validation']['filters']['minCheckbox'] ) && $element['validation']['filters']['minCheckbox'] != '' ) {
					$min_checkboxes = (int) $element['validation']['filters']['minCheckbox'];
				}
				if ( isset( $element['validation']['filters']['maxCheckbox'] ) && $element['validation']['filters']['maxCheckbox'] != '' ) {
					$max_checkboxes = (int) $element['validation']['filters']['maxCheckbox'];
				}
			}

			$total_checkboxes = rand( $min_checkboxes, $max_checkboxes );
			for ( $i = 0; $i < $total_checkboxes; $i++ ) {
				$data['options'][] = array_pop( $option_keys );
			}
		} else {
			$data['options'][] = array_pop( $option_keys );
		}

		if ( in_array( 'others', $data['options'] ) ) {
			$data['others'] = $this->get_random_text();
		}
		return $data;
	}

	protected function get_random_date( $past = '', $future = '', $type = 'datetime' ) {
		if ( $type == 'time' ) {
			return rand( 0, 24 ) . ':' . rand( 0, 59 ) . ':' . rand( 0,59 );
		}
		$str = 'Y-m-d';
		if ( $type == 'datetime' ) {
			$str .= ' H:i:s';
		}
		$past_date = current_time( 'timestamp' );
		$future_date = $past_date - rand( 86400, 86400 * 5 );

		if ( $past != '' ) {
			if ( strtolower( $past ) == 'now' ) {
				$past_date = current_time( 'timestamp' );
			} else {
				$past_date = strtotime( $past );
			}
		}
		if ( $future != '' ) {
			if ( strtolower( $future ) == 'now' ) {
				$future_date = current_time( 'timestamp' );
			} else {
				$future_date = strtotime( $future );
			}
		}

		$random_date = rand( $past_date, $future_date );
		return date( $str, $random_date );
	}

	protected function get_random_address() {
		return array(
			'recipient' => $this->get_random_f_name() . ' ' . $this->get_random_l_name(),
			'line_one' => 'Address Line 1',
			'line_two' => 'Address Line 2',
			'line_three' => 'Address Line 3',
			'country' => 'India',
		);
	}

	protected function get_random_multiline_text() {
		$maxline = rand( 0, 5 );
		$string = $this->get_random_text();
		for ( $i = 0; $i < $maxline; $i++ ) {
			$string .= "\n\r\n\r" . $this->get_random_text();
		}
		return $string;
	}

	protected function get_random_password( $length = 20 ) {
		if ( $length < 1 ) {
			$length = 1;
		}
		$small_bits = 'qwertyuiopasdfghjklzxcvbnm';
		$cap_bits = 'MNBVCXZASDFGHJKLPOIUYTREWQ';
		$numbers = '1234567890';
		$special_characters = '-=+_)(*&^%$#@!:;,./?\'"~`{}[]|\\';
		$entropy = array(
			0 => $small_bits,
			1 => $cap_bits,
			2 => $numbers,
			3 => $special_characters,
		);

		$password = '';
		for ( $i = 0; $i < $length; $i++ ) {
			$en_to_use = $entropy[ rand( 0, count( $entropy ) - 1 ) ];
			$password .= substr( $en_to_use, rand( 0, strlen( $en_to_use ) - 1 ), 1 );
		}
		return $password;
	}


	protected function get_random_limited_text( $min = '', $max = '', $type = 'string' ) {
		$min = trim( $min );
		$max = trim( $max );
		if ( $min == '' ) {
			$min = 10;
		}
		if ( $max == '' ) {
			$max = 100;
		}
		if ( $max <= $min ) {
			$max = $min * 2;
		}

		if ( $type == 'string' ) {
			$string = $this->get_random_text();
		} else {
			$string = rand( 0, 9 );
		}

		while ( strlen( $string ) < $min ) {
			if ( $type == 'string' ) {
				$string .= ' ' . $this->get_random_text();
			} else {
				$string .= ' ' . rand( 0, 9 );
			}
		}

		if ( strlen( $string ) > $max ) {
			$string = substr( $string, 0, $max );
		}
		return $string;
	}

	protected function get_random_number( $min = '', $max = '' ) {
		$min = trim( $min );
		$max = trim( $max );
		if ( $min == '' ) {
			$min = 0;
		}
		if ( $max == '' ) {
			$max = 100;
		}
		if ( $max <= $min ) {
			$max = $min * 2;
		}
		return rand( $min, $max );
	}

	protected function get_random_ip() {
		return rand( 0, 255 ) . '.' . rand( 0, 255 ) . '.' . rand( 0, 2 ) . '.' . rand( 0, 255 );
	}

	protected function get_random_phone() {
		return rand( 80, 99 ) . rand( 30, 40 ) . rand( 10, 99 ) . rand( 10, 99 ) . rand( 10, 99 );
	}

	protected function get_random_f_name() {
		$random_f_name = array(
			'Jacob',
			'Michael',
			'Joshua',
			'Matthew',
			'Daniel',
			'Hannah',
			'Olivia',
			'Emma',
			'Madison',
			'Emily',
			'Christopher',
			'Abigail',
			'Andrew',
			'Isabella',
			'Ethan',
			'Samantha',
			'Joseph',
			'Elizabeth',
			'William',
			'Ashley',
			'Anthony',
			'Alexis',
			'David',
			'Sarah',
			'Sophia',
			'Alexander',
			'Nicholas',
			'Alyssa',
			'Ryan',
			'Grace',
			'Tyler',
			'Ava',
		);
		return $random_f_name[ rand( 0, count( $random_f_name ) - 1 ) ];
	}

	protected function get_random_l_name() {
		$random_l_name = array(
			'Smith',
			'Johnson',
			'Williams',
			'Jones',
			'Brown',
			'Davis',
			'Miller',
			'Wilson',
			'Moore',
			'Taylor',
			'Anderson',
			'Thomas',
			'Jackson',
			'White',
			'Harris',
			'Martin',
			'Thompson',
			'Garcia',
			'Martinez',
			'Robinson',
			'Clark',
		);
		return $random_l_name[ rand( 0, count( $random_l_name ) - 1 ) ];
	}

	protected function get_random_name() {
		return $this->get_random_f_name() . ' ' . $this->get_random_l_name();
	}

	protected function get_random_e_domain() {
		$random_e_domain = array(
			'abc.com',
			'xyz.com',
			'qwerty.com',
			'myhealth.biz',
			'naming.org',
			'live.com',
			'gmail.com',
			'yahoo.com',
			'balbber.com',
			'domain.in',
		);
		return $random_e_domain[ rand( 0, count( $random_e_domain ) - 1 ) ];
	}

	protected function get_random_url() {
		return 'http://' . $this->get_random_e_domain();
	}

	protected function get_random_email() {
		return strtolower( $this->get_random_f_name() ) . '@' . $this->get_random_e_domain();
	}

	protected function get_random_text() {
		$random_texts = array(
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
			'Suspendisse eu luctus purus.',
			'Nam ut ante arcu',
			'Suspendisse at sem nisl',
			'Nullam dignissim ultrices fermentum',
			'Sed cursus consequat risus et porttitor',
			'Suspendisse cursus posuere est',
			'Integer posuere euismod lorem',
			'Sed interdum lorem nec nulla euismod sit amet luctus dui placerat',
			'Nunc interdum, nunc quis dapibus dignissim',
			'Vivamus rutrum nisl quis leo tristique eu auctor nulla hendrerit',
			'Aenean cursus lacus in lectus interdum a porta est porttitor.',
			'Etiam a ornare mauris',
			'Suspendisse libero libero',
			'Praesent aliquet, ligula et consequat venenatis',
			'Nullam faucibus enim id tortor molestie non tristique magna suscipit',
			'Mauris fermentum egestas neque',
			'Phasellus sit amet sem nec mauris blandit ornare.',
			'Cras orci neque, rutrum a posuere at',
			'Phasellus nulla eros, semper quis gravida id',
			'Pellentesque pellentesque aliquam justo',
		);
		return $random_texts[ mt_rand( 0, count( $random_texts ) - 1 ) ];
	}

	protected function get_random_thumbselect( $element ) {
		// Simply get one or more keys from the options
		// depending on the multiple option
		if ( true === $element['settings']['multiple'] ) {
			return array_map( 'strval', (array) array_rand( $element['settings']['options'], mt_rand( 1, count( $element['settings']['options'] ) ) ) );
		} else {
			return array_map( 'strval', (array) array_rand( $element['settings']['options'], 1 ) );
		}
	}

	protected function get_random_gps() {
		$data = array();
		$data[] = array(
			'lat' => '27.1750151',
			'long' => '78.04215520000002',
			'location_name' => 'Dashehara Ghat Rd, Dharmapuri, Forest Colony, Tajganj, Agra, Uttar Pradesh 282006, India',
		);
		$data[] = array(
			'lat' => '28.5244281',
			'long' => '77.18545589999997',
			'location_name' => 'Qutab Minar, Seth Sarai, Mehrauli, New Delhi, Delhi 110016, India',
		);
		$data[] = array(
			'lat' => '22.5448082',
			'long' => '88.34255780000001',
			'location_name' => 'Maidan, Kolkata, West Bengal 700071, India',
		);
		$data[] = array(
			'lat' => '27.1795328',
			'long' => '78.02111200000002',
			'location_name' => 'Musamman Burj, SH 62, Agra Fort, Rakabganj, Agra, Uttar Pradesh 282003, India',
		);
		$data[] = array(
			'lat' => '28.5933079',
			'long' => '77.2507008',
			'location_name' => 'Humayun\'s Tomb, Nizamuddin, Nizamuddin East, New Delhi, Delhi 110013',
		);
		shuffle( $data );
		return array_pop( $data );
	}

	protected function ajax_options() {
		$buttons = array(
			array( __( 'Generate Demo Data', 'ipt_fsqm' ), '', 'large', 'primary', 'normal', array(), 'submit' ),
			array( __( 'Reset', 'ipt_fsqm' ), '', 'small', 'secondary', 'normal', array(), 'reset' ),
		);
		$forms = IPT_FSQM_Form_Elements_Static::get_forms();
		if ( null == $forms ) {
			$this->ui->msg_error( __( 'You have not setup any forms.', 'ipt_fsqm' ) );
			return;
		}
		$select_forms = array();

		foreach ( $forms as $form ) {
			$select_forms[] = array(
				'label' => $form->name,
				'value' => $form->id,
			);
		}
		?>
<?php $this->ui->msg_error( __( 'Warning: Once you generate demo data using this tool you can not delete them automatically. It is always a good idea to backup your database before continuing.', 'ipt_fsqm' ) ); ?>
<form action="" method="post">
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row"><?php $this->ui->generate_label( 'ipt_fsqm_demo_form_id', __( 'Select a form', 'ipt_fsqm' ) ); ?></th>
				<td>
					<?php $this->ui->select( 'ipt_fsqm_demo_form_id', $select_forms, 0 ); ?>
				</td>
				<td>
					<?php $this->ui->help( __( 'Select the form to which you wish to generate demo data.', 'ipt_fsqm' ) ); ?>
				</td>
			</tr>
			<tr>
				<th scope="col"><?php $this->ui->generate_label( 'ipt_fsqm_demo_past_days', __( 'Number of past days to calculate for', 'ipt_fsqm' ) ); ?></th>
				<td>
					<?php $this->ui->slider( 'ipt_fsqm_demo_past_days', '2', '0', '100' ); ?>
				</td>
				<td>
					<?php $this->ui->help( __( 'Includes Today as well. So, if you enter 10, then past 9 days, including today\'s will be submitted.', 'ipt_fsqm' ) ); ?>
				</td>
			</tr>
			<tr>
				<th scope="col"><?php $this->ui->generate_label( 'ipt_fsqm_demo_max', __( 'Maximum number of submissions to generate/day', 'ipt_fsqm' ) ); ?></th>
				<td>
					<?php $this->ui->spinner( 'ipt_fsqm_demo_max', '10', '', '1', '' ); ?>
				</td>
				<td>
					<?php $this->ui->help( __( 'Select the maximum number of submissions the system will generate. The actual generated maybe less.', 'ipt_fsqm' ) ); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<?php $this->ui->buttons( $buttons ); ?>
</form>
		<?php
	}
}
