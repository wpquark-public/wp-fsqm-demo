# eForm Demo Data Generator

> This plugin aims to provide sample submissions for any eForm Managed form. Currently it can generate submission for any inbuilt form elements.

If you want to add supprot for your element, then you are welcome to fork, commit and make a pull request.

Coding should strictly obey [WordPress Coding Standards](https://make.wordpress.org/core/handbook/best-practices/).

The plugin comes with a sample Demo Generator which you can use to populate the database for testing purpose.

### Installation

To install it, simply follow the steps:

1. Unzip & upload the wp-fsqm-demo.zip file located under /Demo Generator/ Directory to your wp-content/plugins/ directory (via FTP) or Go to Plugins > Add New > Upload and upload the same file (the zip file).
2. Activate the plugin. This will add a new menu under eForm named Demo Generator.
3. Go to eForm > Demo Generator and enter the number of days you wish to generate demo and submit.
4. Unlike before the demo generator will not create any sample forms of its own, rather it will use any form that you mention.

Here are a few things to note before using it:


* You can select any form to populate submission as long as the form is using just the core elements (the ones supplied with eForm, not some third party extensions).
* It will randomly pick a number between 10 and 20 and will populate that amount of submissions per day, for the number of days you have selected.
* It will not remember any data it has generated.
* Those are permanent, just like any actual submission.
* All the data will be submitted under Guest account.
* For payment element, the system assumes that you have enabled stripe payment setup with a test API key.

To delete the generated demo data, simply delete the above mentioned forms. It will delete the forms along with their data.
You can then, optionally deactivate and delete the demo plugin to remove every last trace of the plugin.
